/*! jquery lazy load v1.0.0 06.08.2019
 * https://bitbucket.org/no-bugs/jquery-lazy-load/src/master/
 *
 * Copyright (c) 2019 No Bugs;
 * Licensed under the MIT license */
(function ( $ ) {
    $.fn.lazyLoad = function(options) {
        var $items;
        var _options = {
            'scroll_margin': 50
        };
        var $body = $('body');
        var $window = $(window);
        var $document = $(document);
        var scroll_timer;

        _options = $.extend({}, _options, options);
        $items = this;
        $items = $items.toArray();
        if($items.length) {
            $window.scroll(onScroll);
            $window.scroll();
        }
        function onScroll() {
            if(!scroll_timer) {
                scroll_timer = setTimeout(function() {
                    var src;
                    var window_height = $window.height();
                    var scroll_top = $document.scrollTop();
                    $items = $items.filter(function(item, index, arr) {
                        if(src = item.dataset['src']) {
                            var offset = $(item).offset();
                            if(offset.top > scroll_top - _options.scroll_margin && offset.top < window_height + scroll_top + _options.scroll_margin) {
                                item.src = src;
                                delete item.dataset['src'];
                                return false;
                            }
                        } else {
                            return false;
                        }
                        return true;
                    });
                    if(!$items.length) {
                        $window.unbind('scroll',onScroll);
                    }
                    scroll_timer = null;

                }, 100);
            }
        }
    };
})(jQuery);
