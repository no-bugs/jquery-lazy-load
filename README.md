### How to use
Add jquery.lazy-load.min.js after jquery library.
For images that must be loaded when scrolling set data-src attribute, for src can be set 1x1.png.
Use selector for adding lazy load event on images, as sample used class lazy:
```
<img src="1x1.png" data-src="some-image.jpg" alt="some image" class="lazy">
<script>
    $('.lazy').lazyLoad();
</script>
```
The picture will start loading when it is at a distance of 50px or less from the visible area of the screen. You can change this value:
```
<script>
    $('.lazy').lazyLoad({
        scroll_margin: 200px
    });
</script>
```
